# vectorizing first 100 users.
# TODO label 101-200 users. due to typo they didn't get into marking queue
# 2015-01-21. I don't understand TODO. ~~george

import pickle
import numpy as np
from scipy.sparse import dok_matrix

fl = open("../data/social_graph_weights.txt").readlines()[:1003]
d = {}

for line in fl:
    sl = line.replace("\xef\xbb\xbf",'').replace("\xc2\xa0",'').split(" ")
    tid = int(sl[0].split(" ")[0].strip())
    if sl[0] == sl[-1]:
        print "skipping unmarked node"
        continue
    try:
        d[tid] = int(sl[-1])
    except:
        print "!", sl[-1]
        d[tid] = 0

t2i = pickle.load(open("t2i.pkl")) 
n = len(t2i.keys())
v = dok_matrix((n,1))

for tid in d.keys():
    if tid not in t2i:
        print "missing vertex, probably has no friends"
        continue
    v[t2i[tid],0] = d[tid]


pickle.dump(v, open("v900rand.pkl","w"))
np.save(open('v900rand.npz','w'), v)
print len(v.keys())
