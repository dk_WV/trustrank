import pickle

file_path = "../data/social_graph350k.txt"
data = open(file_path).readlines()
t2i = {}
i2t = {}

i = 0
for x in data:
    t_ids = map(lambda x: int(x), x.split(" "))
    t_id = t_ids[0]
    if t_id not in t2i:
            t2i[t_id] = i
            i2t[i] = t_id
            i = i + 1

from scipy.sparse import dok_matrix
import numpy as np

n = len(t2i.keys())
m = len(t2i.values())

print "t2i number of keys must be equal to the number of values", n,m, n==m
print "i2t, -//-", len(i2t.keys()), len(i2t.values())

A = dok_matrix((n,n), dtype=np.int8)

for x in data:
    t_ids = map(lambda x: int(x), x.split(" "))
    source = t2i[t_ids[0]]
    for t_id in t_ids[1:]:
        if not t_id in t2i:
            continue
        dest = t2i[t_id]
        #print dest, source
        A[dest, source] = 1
        
pickle.dump(A.tocsc(),   open('A.pkl','w'))
pickle.dump(i2t, open('i2t.pkl','w'))
pickle.dump(t2i, open('t2i.pkl','w'))
