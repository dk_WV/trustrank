import gevent
import itertools
import time

import numpy as np
import scipy.sparse as sp

test_on_bvg = False

if not test_on_bvg:    
    n_edges = 200000000

    import gevent.threadpool
    def computation(edges_and_degrees, thread_id, max_threads):
        def comp():
            pass
        
        def get_indexes(thread_id, max_threads):
            return (n_edges / (max_threads) * thread_id, 
                    n_edges / (max_threads) * (thread_id + 1))
        
        (left_bound, right_bound) = get_indexes(thread_id, max_threads)
        edge_number = 0
        for i in edges_and_degrees:
            if edge_number < left_bound:
                # We have to it via all indexes.
                continue 
             
            if edge_number > right_bound:  
                break
             
            comp()
             
            edge_number += 1
            
    edges_and_degrees = range(n_edges)    
    #gevent.joinall([
    #    gevent.spawn(computation, edges_and_degrees, 0, 2),
    #    gevent.spawn(computation, edges_and_degrees, 0, 2),
    #])
    
    start_time = time.time()
    n_processes = 8
    tp = gevent.threadpool.ThreadPool(n_processes)
    
    for i in xrange(n_processes):
        tp.spawn(computation, edges_and_degrees, 0, n_processes)
    
    gevent.wait()
    
    
    
if test_on_bvg:
    import bvg
    # Example for bvg.
    n_iter = 5
    st = time.time()
    G = bvg.BVGraph('/media/rdkl/data/trust_rank/uk-2007-05') 
    print "Load time: %.2f" % (time.time() - st)
    st = time.time()
    edges_and_degrees = G.edges()
    
    n_edges = G.nedges        
    
    # Variants:
    #    1. Left-right boundaries
    #    2. Every thread will do only every max_threads edge.
    #    3. Copy part of file using itertools to every thread (may consume memory) 
    
    
    def computation(edges_and_degrees, thread_id, max_threads):
        def get_indexes(thread_id, max_threads):
            return (n_edges / (max_threads) * thread_id, 
                    n_edges / (max_threads) * (thread_id + 1))
        def explore_edge(src, dst):
            pass
        
        (left_bound, right_bound) = get_indexes(thread_id, max_threads)
        for iter in xrange(n_iter):
            edge_number = 0
            for (src, dst) in edges_and_degrees:
                if edge_number < left_bound:
                    # We have to it via all indexes.
                    continue 
                
                if edge_number > right_bound:  
                    break
                
                explore_edge(src, dst)
                
                edge_number += 1
                
    gevent.joinall([
        gevent.spawn(computation(edges_and_degrees, 0, 2)),
        gevent.spawn(computation(edges_and_degrees, 0, 2)),
    ])
