# vectorizing first 100 users.
# TODO label 101-200 users. due to typo they didn't get into marking queue

import pickle
import numpy as np
from scipy.sparse import dok_matrix

fl1 = open("../data/putinzi.txt").readlines()
fl2 = open("../data/labels_part1.txt").readlines()
p = {}

for line in fl1:
    sl = line.replace("\xef\xbb\xbf",'').replace("\xc2\xa0",'').split(" ")
    tid = int(sl[0].split(" ")[0].strip())
    try:
        p[tid] = int(sl[-1])
    except:
        print "!", sl[-1]
        p[tid] = 0

l = {}
for line in fl2:
    sl = line.replace("\xef\xbb\xbf","").replace("\xc2\xa0","").split("-")
    tid = int(sl[0])
    try:
        l[tid] = float(sl[1])
    except:
        print "!", sl[1]
        l[tid] = 0

t2i = pickle.load(open("t2i.pkl")) 
n = len(t2i.keys())
v = dok_matrix((n,1))

for tid in p.keys():
    if p[tid] != 0:
        if l[tid] == 0:
            print tid, "inconsistent"
        else:
            v[t2i[tid],0] = 1

for tid in l.keys():
    if (l[tid] != 0) and (p[tid] == 0):
        print tid

np.save(open('v100.npz','w'), v)
