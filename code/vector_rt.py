import pickle
import numpy as np
from scipy.sparse import dok_matrix

t2i = pickle.load(open("t2i.pkl")) 
n = len(t2i.keys())

def vectorize(filename):
    fl = open(filename).readlines()
    d = {}

    for line in fl:
        sl = line.split(',')
        if len(sl[0]) == 0:
            continue
        tid = int(sl[0])
        if len(sl[1]) == 0:
            continue
        if sl[1] == '1':# or sl[1] == "0,5":
            d[tid] = 1
        else:
            d[tid] = -1
    v = dok_matrix((n,1))

    for tid in d.keys():
        if tid not in t2i:
            print "missing vertex, probably has no friends"
            continue
        v[t2i[tid],0] = d[tid]
    
    return v, d

filenames = [
    "../data/rtwitter.1.csv",
    "../data/rtwitter.2.csv",
    "../data/rtwitter.3.csv",
    "../data/rtwitter.4.csv",
        ]

v0 = dok_matrix((n,1))
dl = []

for fx in filenames:
    f = fx.split('/')[-1] + '.pkl'
    v, d = vectorize(fx)
    dl.append(set(d))

    v0 = v + v0
    #print "dry run, not saving results"
    print v0.nnz
    #np.save(open(f,'w'), v)
    pickle.dump(v, open(f,'w'))
