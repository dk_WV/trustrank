#-*- coding: utf-8 -*-

import copy
import pickle

import numpy as np
from trust_distrust_rank import trust_distrust_rank as algorithm

np.set_printoptions(3)

if __name__ == "__main__":
    try:
        f = open("A.pkl")
        graph_matrix = pickle.load(f)
        f.close()    
    except:
        print "Encountered problems with file A.pkl (can't load A from it)."
    
    n = graph_matrix.shape[0]
    
    # Names of files that contains manually labeled data.
    names = ["v900rand.pkl", 
             "rtwitter.1.csv.pkl",
             "rtwitter.2.csv.pkl",
             "rtwitter.3.csv.pkl",
             "rtwitter.4.csv.pkl",
             ] 
    
    labeled_data = np.zeros(n)
    
    for name in names:
        temp = np.zeros(n)
        try:
            temp[:] = pickle.load(open(name)).todense().flatten()
        except:
            print "Can not load file", name
        
        labeled_data += temp
    
    n_folds = 2
    
    good_set = np.nonzero(labeled_data > 0)[0].tolist()
    bad_set = np.nonzero(labeled_data < 0)[0].tolist()
 
    np.random.shuffle(good_set)
    np.random.shuffle(bad_set)
  
    fold_size_good = len(good_set) / n_folds
    fold_size_bad = len(bad_set) / n_folds
   
    for i in xrange(n_folds):
        if (i != n_folds - 1):
            good_set_indices = good_set[i * fold_size_good:
                                          (i + 1) * fold_size_good]
            bad_set_indices = bad_set[i * fold_size_bad:
                                          (i + 1) * fold_size_bad]
        else:
            good_set_indices = good_set[(i) * fold_size_good:]
            bad_set_indices = bad_set[(i) * fold_size_bad:]
        
        labeled_data_test_indices = good_set_indices + bad_set_indices
        # print labeled_data_test_indices 
        labeled_data_train = copy.deepcopy(labeled_data)
        labeled_data_train[labeled_data_test_indices] = 0
        
        d, t = algorithm(graph_matrix, labeled_data_train)
        print sum(t[good_set_indices] > d[good_set_indices]), len(good_set_indices)
        print sum(t[bad_set_indices] < d[bad_set_indices]), len(bad_set_indices)
        